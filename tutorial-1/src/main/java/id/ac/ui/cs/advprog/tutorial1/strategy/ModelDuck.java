package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    public ModelDuck(){
        flyBehavior = new FlyNoWay();
        quackBehavior = new Squeak();
    }

    @Override
    public void display() {
        System.out.println("Model duck");
    }

    @Override
    public void swim() {
        System.out.println("Swim");
    }
}
